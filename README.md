# What is IFOS2D?


**IFOS2D** (**I**nversion of **F**ull **O**bserved **S**eismograms) is a 2-D elastic full waveform inversion code.  
The inversion problem is solved by a conjugate-gradient method and the gradients are computed in the time domain by the adjoint-state method.  
The forward modeling is done by a time domain finite-difference scheme.

IFOS2D is the reverse (inverse) of our 2D finite-difference forward solver [**SOFI2D**].


